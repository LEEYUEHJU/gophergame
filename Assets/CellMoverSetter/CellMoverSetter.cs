﻿public  class CellMoverSetter : ICellMoverSetter
{
	private CellMover cellMover_;
	private Grid grid_;
	private ICellMoverDistributor cellMoverDistributor_;
	private float speed_;

	public CellMoverSetter(ICellMoverDistributor cellMoverDistributor){
		cellMoverDistributor_ = cellMoverDistributor;
	}

	public void MoveBrickFromCellToCell_(Cell fromCell,Cell toCell){
		CellMover cellMover = cellMoverDistributor_.GetCellMover ();
		cellMover.MoveBrickFromCellToCell (fromCell,toCell);
		cellMover.SetSpeed (speed_);
	}

	public void SetSpeed(float speed){
		speed_ = speed;
	}

}
