﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class CellMoverSetterTester {


	private CellMoverSetter cellMoverSetter_;
	private CellMoverDistributorMock cellMoverDistributorMock_;

	[SetUp]
	public void Init(){
		cellMoverDistributorMock_ = new CellMoverDistributorMock ();
		cellMoverSetter_ = new CellMoverSetter (cellMoverDistributorMock_);
	}
}
