﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICellMoverSetter  {
	
	void MoveBrickFromCellToCell_ (Cell fromCell, Cell toCell);
	void SetSpeed (float speed);
}
