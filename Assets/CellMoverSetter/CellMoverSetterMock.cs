﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellMoverSetterMock : ICellMoverSetter {

	Cell fromCell_;
	Cell toCell_;

	public  void MoveBrickFromCellToCell_ (Cell fromCell, Cell toCell){
		fromCell_ = fromCell;
		toCell_ = toCell;
	}

	public Cell[] GetFromToCells(){
		return new Cell[]{ fromCell_, toCell_ };
	}

	public void SetSpeed(float speed){
	
	}
}
