﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellsAndBricksSetter  {
	private Cell[] cells_;
	private Brick[] bricks_;

	public CellsAndBricksSetter(Cell[]cells,Brick[]bricks){
		CheckValidation_ (cells,bricks);
		cells_ = cells;
		bricks_ = bricks;
		SetBricksIntoCells_ ();
	}

	private void CheckValidation_(Cell[]cells,Brick[]bricks){
		if(cells.Length<bricks.Length){
			throw new System.ArgumentException ();
		}
	}

	public void SetColumnMajorPosition(){
		SetCellsInColumnMajorPosition_ ();
		SetBricksPosition_ ();
	}

	public void SetRowMajorPosition(){
		SetCellsInRowMajorPosition_ ();
		SetBricksPosition_ ();
	}

	private void SetCellsInColumnMajorPosition_(){
		for (int column= 0; column < cells_.Length; column++) {
			cells_ [column].SetPosition (new Vector3(0,-column,0));
		}
	}

	private void SetCellsInRowMajorPosition_(){
		for (int row = 0; row < cells_.Length; row++) {
			cells_ [row].SetPosition (new Vector3(row,0,0));
		}
	}

	private void SetBricksPosition_(){
		for (int i = 0; i < bricks_.Length; i++) {
			bricks_ [i].SetPosition (cells_[i].GetPosition());
		}
	}

	private void SetBricksIntoCells_(){
		for (int row= 0; row < bricks_.Length; row++) {
			cells_ [row].SetContent (bricks_[row]);
		}
	}

}
