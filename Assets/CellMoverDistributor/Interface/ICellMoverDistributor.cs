﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICellMoverDistributor  {

	 CellMover GetCellMover();
}
