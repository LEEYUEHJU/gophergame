﻿

public class CellMoversDistributor : ICellMoverDistributor{

	private CellMoversQueue cellMoversQueue_;

	public CellMoversDistributor(CellMoversQueue cellMoverQueue,CellMover[]cellmovers){
		cellMoversQueue_ = cellMoverQueue;
		RegisterRecycleEvent_ (cellmovers);
	}

	private void RegisterRecycleEvent_(CellMover[]cellmovers){
		for (int i = 0; i < cellmovers.Length; i++) {
			cellmovers [i].RegisterArriveEvent (Recycle);
		}
	}

	private void Recycle(CellMover cellMover){
		cellMoversQueue_.Recycle (cellMover);
	}

	public CellMover GetCellMover(){
		return cellMoversQueue_.GetCellMover ();
	}
}
