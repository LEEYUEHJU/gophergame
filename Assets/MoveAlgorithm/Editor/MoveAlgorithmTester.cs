﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class MoveAlgorithmTester {


	private MoveAlgorithm moveAlgorithm_ = null;

	[SetUp]
	public void Init(){
		moveAlgorithm_ = new MoveAlgorithm ();
	}

	[Test]
	public void MovePositionWithTime_MoveFrom0n3n0To0n0n0WithSpeed2InDurationOf1_ReachedPosition0n1n0(){
		Vector3 fromPosition =  new Vector3(0,3,0);
		Vector3 toPosition =  (new Vector3(0,0,0));
		moveAlgorithm_.SetSpeed (2f);
		float duration = 1f;
		Assert.AreEqual (new Vector3(0,1,0),moveAlgorithm_.GetUpdatedPosition (fromPosition,toPosition,duration));
	}

	[Test]
	public void MovePositionWithTime_MoveFrom0n3n0To0n0n0WithSpeed3InDuration1_ReachedPosition0n1n0(){
		Vector3 fromPosition =  new Vector3(0,3,0);
		Vector3 toPosition =  (new Vector3(0,0,0));
		moveAlgorithm_.SetSpeed (3f);
		float duration = 1f;
		Assert.AreEqual (new Vector3(0,0,0),moveAlgorithm_.GetUpdatedPosition (fromPosition,toPosition,duration));
	}

	[Test]
	public void MovePositionWithTime_CantMoveOutOfDestination_ReachedPosition0n0n0(){
		Vector3 fromPosition =  new Vector3(0,3,0);
		Vector3 toPosition =  (new Vector3(0,0,0));
		moveAlgorithm_.SetSpeed (1f);
		float duration = 4f;
		Assert.AreEqual (new Vector3(0,0,0),moveAlgorithm_.GetUpdatedPosition (fromPosition,toPosition,duration));
	}

	[Test]
	public void MovePositionWithTime_MoveFrom0n3n0To0n0n0WithSpeed1InDurationOf1Point1_ReachedPosition0nPoint8n0(){
		Vector3 fromPosition =  new Vector3(0,3,0);
		Vector3 toPosition =  new Vector3(0,0,0);
		moveAlgorithm_.SetSpeed (2);
		float duration = 1.1f;
		Vector3 algorithmResult = moveAlgorithm_.GetUpdatedPosition (fromPosition, toPosition, duration);

		Vector3 calculateRresult = fromPosition+((toPosition - fromPosition).normalized * duration * 2);
		//Vector3 calculateRresult = new Vector3(0,0.8f,0);

		float distance = Vector3.Distance (algorithmResult, calculateRresult);
		Assert.Less (distance,1);
	}
}
