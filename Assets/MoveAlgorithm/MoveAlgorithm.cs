﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAlgorithm  {

	float speed_ = 0;

	public void SetSpeed(float speed){
		speed_ = speed;
	}

	public Vector3 GetUpdatedPosition(Vector3 fromPosition,Vector3 toPosition, float time){
		Vector3 position = Vector3.MoveTowards (fromPosition,toPosition,time*speed_);
		return position;
	}
		
}
