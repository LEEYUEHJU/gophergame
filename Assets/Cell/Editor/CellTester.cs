﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class CellTester {

	private Cell cell_;
	private Brick brick_;

	[SetUp]
	public void Init(){
		cell_ = new Cell ();
		GameObject brickObject = new GameObject ();
		brick_ = brickObject.AddComponent<Brick> ();
	}

	[Test]
	public void GetContent_NotSetAnyContent_IsNull(){
		Assert.IsNull (cell_.GetContent());
	}

	[Test]
	public void GetContent_SetContentOfBrick_ReturnBrick(){
		cell_.SetContent (brick_);
		Assert.AreEqual (brick_,cell_.GetContent());
	}

	[Test]
	public void IsEmpty_CellNotSetAnyContent_ReturnTrue(){
		Assert.AreEqual (true,cell_.IsEmpty());
	}

	[Test]
	public void IsEmpty_SetContentOfBrick_ReturnTrue(){
		cell_.SetContent (brick_);
		Assert.AreEqual (false,cell_.IsEmpty());
	}

	[Test]
	public void IsEmpty_SetContentOfBrickAndRemoveContent_ReturnTrue(){
		cell_.SetContent (brick_);
		cell_.RemoveContent ();
		Assert.AreEqual (true,cell_.IsEmpty());
	}
}
