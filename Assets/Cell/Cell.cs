﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell  {

	private Brick brick_;
	private Vector3 position_;
	private string id_;

	public void SetContent(Brick brick){
		brick_ = brick;
		//brick_.SetPosition (position_);
	}

	public Brick GetContent(){
		return brick_;
	}

	public bool IsEmpty(){
		return null == brick_;
	}

	public void RemoveContent(){
		brick_ = null;
	}

	public void SetPosition(Vector3 pos){
		position_ = pos;
	}

	public Vector3 GetPosition(){
		return position_;
	}

	public void SetID(string id){
		id_ = id;
	}

	public string GetID(){
		return id_;
	}
}
