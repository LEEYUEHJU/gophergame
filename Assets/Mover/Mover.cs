﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Mover {

	private MoveAlgorithm moveAlgorithm_;
	private Vector3 destinatioin_;
	private Vector3 fromPosition_;
	private Transform transform_;
	private bool startMove_;
	private Action arrivedEvent_;

	public Mover(MoveAlgorithm moveAlgorithm){
		moveAlgorithm_ = moveAlgorithm;
	}
		
	public void SetTransform(Transform transFrom){
		transform_ = transFrom;
	}

	public void SetFromPosition(Vector3 posotion){
		fromPosition_ = posotion;
	}

	public void SetDestination(Vector3 destination){
		destinatioin_ = destination;
	}

	public void SetSpeed(float speed){
		moveAlgorithm_.SetSpeed (speed);
	}

	public void StartToMove(){
		startMove_ = true;
	}

	public void OnUpdate(float deltaTime){
		if(startMove_){
			MoveTransformPosition_ (deltaTime);
			InvokeArrivedEventAndStopMoveIfArrived_ ();
		}
	}

	private void MoveTransformPosition_(float deltaTime){
		Vector3 currentPos =  moveAlgorithm_.GetUpdatedPosition (fromPosition_ ,destinatioin_,deltaTime);
		fromPosition_ = currentPos;
		SetTransformPosition_ (fromPosition_);
	}

	private void SetTransformPosition_(Vector3 currentPosition){
		if(null!=transform_){
			transform_.position = currentPosition;
		}
	}

	private void InvokeArrivedEventAndStopMoveIfArrived_(){
		if (isArrived_ ()) {
			InvokeArriveEvent_ ();
			startMove_ = false;
		}
	}

	private bool isArrived_(){
		return destinatioin_ == fromPosition_;
	}
		
	private void InvokeArriveEvent_(){
		if(null!=arrivedEvent_){
			arrivedEvent_.Invoke ();
		}
	}

	public void RegisterArrivedEvent(Action onArrivedEvent){
		arrivedEvent_ += onArrivedEvent;
	}
}
