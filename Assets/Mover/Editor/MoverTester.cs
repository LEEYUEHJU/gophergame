﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class MoverTester {

	private MoveAlgorithm moveAlgorithm_;
	private Mover mover_;
	private Transform transfrom_;
	private int arrivedCount_;

	private void Arrived(){
		arrivedCount_++;
	}

	private void SetUpMover(){
		moveAlgorithm_ = new MoveAlgorithm ();
		mover_ = new Mover (moveAlgorithm_);
	}

	private void SetUpTransform(){
		var testGameObject = new GameObject ();
		transfrom_ = testGameObject.GetComponent<Transform> ();
	}
		
	[SetUp]
	public void Init(){
		SetUpMover ();
		SetUpTransform ();
		mover_.RegisterArrivedEvent (Arrived);
		mover_.SetTransform (transfrom_);		
		arrivedCount_ = 0;
	}

	[Test]
	public void OnUpdate_MoveTo0n2n0OnUpdate2_TransformPositionIs0n2n0(){
		mover_.SetFromPosition (new Vector3(0,0,0));
		mover_.SetDestination (new Vector3(0,2,0));
		mover_.SetSpeed (1f);
		mover_.StartToMove ();
		mover_.OnUpdate (2f);
		Assert.AreEqual (new Vector3(0,2,0),transfrom_.position);
	}

	[Test]
	public void OnUpdate_MoveTo0n2n0OnUpdate1Point1_TransformPositionIs0n1Point1n0(){
		mover_.SetFromPosition (new Vector3(0,0,0));
		mover_.SetDestination (new Vector3(0,2,0));
		mover_.SetSpeed (1f);
		mover_.StartToMove ();
		mover_.OnUpdate (1.1f);
		Assert.AreEqual (new Vector3(0,1.1f,0),transfrom_.position);
	}

	[Test]
	public void OnUpdate_MoveTo0n2n0OnUpdate1Twice_TransformPositionIs0n2n0(){
		mover_.SetFromPosition (new Vector3(0,0,0));
		mover_.SetDestination (new Vector3(0,2,0));
		mover_.SetSpeed (1f);
		mover_.StartToMove ();
		mover_.OnUpdate (1f);
		mover_.OnUpdate (1f);
		Assert.AreEqual (new Vector3(0,2,0),transfrom_.position);
	}

	[Test]
	public void RegisterArrivedEvent_ArrivedDestination_ArrivedEventTrigger(){
		mover_.SetFromPosition (new Vector3(0,0,0));
		mover_.SetDestination (new Vector3(0,2,0));
		mover_.SetSpeed (1f);
		mover_.StartToMove ();
		mover_.OnUpdate (2f);
		Assert.AreEqual (1,arrivedCount_);
	}

	[Test]
	public void RegisterArrivedEvent_ArrivedDestinationTwice_ArrivedEventTriggerTwice(){
		mover_.SetFromPosition (new Vector3(0,0,0));
		mover_.SetDestination (new Vector3(0,2,0));
		mover_.SetSpeed (1f);
		mover_.StartToMove ();
		mover_.OnUpdate (2f);
		mover_.SetFromPosition (new Vector3(0,2,0));
		mover_.SetDestination (new Vector4(0,4,0));
		mover_.StartToMove ();
		mover_.OnUpdate (2f);
		Assert.AreEqual (2,arrivedCount_);
	}
		
	[Test]
	public void RegisterArrivedEvent_DestinationIsEqualToFromposition_ArrivedEventTrigger(){
		mover_.SetFromPosition (new Vector3(1,0,0));
		mover_.SetDestination (new Vector3(1,0,0));
		mover_.StartToMove ();
		mover_.OnUpdate (1f);
		Assert.AreEqual (1,arrivedCount_);
	}

	[Test]
	public void OnUpdate_NotStartMove_TransformStayAtOrignalPosition(){
		Vector3 originalPosition = transfrom_.position;
		mover_.SetFromPosition (originalPosition);
		mover_.SetDestination (new Vector3(0,2,0));
		mover_.SetSpeed (1f);
		mover_.OnUpdate (1f);
		Assert.AreEqual (originalPosition,transfrom_.position);
	}
		
}
