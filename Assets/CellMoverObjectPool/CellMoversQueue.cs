﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellMoversQueue  {
	
	private Queue queue_;

	public CellMoversQueue(CellMover[]cellmovers){
		queue_ = new Queue ();
		Enqueue_ (cellmovers);
	}

	private void Enqueue_(CellMover[]cellmovers){
		for (int i = 0; i < cellmovers.Length; i++) {
			queue_.Enqueue(cellmovers[i]);
		}
	}

	public void Recycle(CellMover cellMover){
		queue_.Enqueue (cellMover);
	}

	public CellMover GetCellMover(){
		IfEmptyThrowException_ ();
		CellMover mover = (CellMover)queue_.Dequeue ();
		return mover;
	}

	private void IfEmptyThrowException_(){
		if(0 == queue_.Count){
			throw new System.ArgumentException ();	
		}
	}

	public int GetCounts(){
		return queue_.Count;
	}
}
