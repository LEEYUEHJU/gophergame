﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class CellMoverObjectPoolTester {

	private CellsCreator cellsCreator_;
	private CellMover []cellMovers_;
	private Mover[] movers_;
	private CellMoversQueue cellMoverObjectPool_;
	private int MaxCellMoverCounts_ = 5;
	private Cell sourceCell_;
	private Cell destinationCell_;
	private Brick brickOfSourceCell_;
	private CellsAndBricksSetter cellAndBricksSetter_;

	private void CreateMovers_(int moverCount){
		movers_ = new Mover[moverCount];
		MoveAlgorithm algorithm_ = new MoveAlgorithm ();
		for (int i = 0; i < movers_.Length; i++) {
			movers_ [i] = new Mover (algorithm_);
		}
	}

	private void CreateCellMovers_(int cellMoversCount){
		CreateMovers_ (cellMoversCount);
		cellMovers_ = new CellMover[cellMoversCount];
		for (int i = 0; i < cellMovers_.Length; i++) {
			cellMovers_ [i] = new CellMover (movers_[i]);
		}
	}

	private void RunOutCellMoverPool_(){
		for (int i = 0; i < MaxCellMoverCounts_; i++) {
			cellMoverObjectPool_.GetCellMover ();
		}
	}

	[SetUp]
	public void Init(){
		CreateCellMovers_ (MaxCellMoverCounts_);
		cellMoverObjectPool_ = new CellMoversQueue (cellMovers_);
	}
		
	[Test]
	public void GetCellMover_GetCounts_TotalCountsMinus1(){
		CellMover cellmover = cellMoverObjectPool_.GetCellMover ();
		Assert.AreEqual (MaxCellMoverCounts_-1,cellMoverObjectPool_.GetCounts());
	}

	[Test]
	public void GetCellMover_CellMoverPoolIsRunningOut_ThrowException(){
		RunOutCellMoverPool_ ();
		Assert.Throws<System.ArgumentException>(()=>cellMoverObjectPool_.GetCellMover());
	}

	[Test]
	public void GetCellMover_Recycle_ReturnMaxCellMoverCounts(){
		CellMover cellmover = cellMoverObjectPool_.GetCellMover ();
		cellMoverObjectPool_.Recycle (cellmover);
		Assert.AreEqual (MaxCellMoverCounts_,cellMoverObjectPool_.GetCounts());
	}
}

