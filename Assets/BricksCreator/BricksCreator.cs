﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BricksCreator   {

	private int numbersOfRows_;
	private int numbersOfColumns_;
	private GameObject brickPrefab_;
	private Brick[] columnOfBricks_;
	private Brick[] rowOfBricks_;
	private Brick[,] bricks_;

	public BricksCreator(GameObject brickPrefab,int numbersOfRows,int numbersOfColumn){
		brickPrefab_ = brickPrefab;
		numbersOfRows_ = numbersOfRows;
		numbersOfColumns_ = numbersOfColumn;
	}

	private void CreateBricks_(Brick[]bricks){
		for (int i = 0; i < bricks.Length; i++) {
			GameObject BrickObject = (GameObject)Object.Instantiate (brickPrefab_);
			bricks [i] = BrickObject.AddComponent<Brick>();
		}
	}

	public Brick[] CreateColumn(){
		columnOfBricks_ = new Brick[numbersOfRows_];
		CreateBricks_ (columnOfBricks_);	
		return columnOfBricks_;
	}

	public Brick[] CreateRow(){
		rowOfBricks_ = new Brick[numbersOfColumns_];
		CreateBricks_ (rowOfBricks_);	
		return rowOfBricks_;
	}

	private void CreateBricks_(){
		bricks_ = new Brick[numbersOfColumns_,numbersOfRows_];
		for (int row = 0; row < numbersOfRows_; row++) {
			CreateRowOfCells_ (row);
		}
	}

	private void CreateRowOfCells_(int row){
		for (int column = 0; column < numbersOfColumns_; column++) {
			bricks_ [column, row] = new Brick ();
		}
	}

	public Brick[,] Create(){
		if(null!=bricks_){
			CreateBricks_ ();
		}	
		return bricks_;
	}
}
