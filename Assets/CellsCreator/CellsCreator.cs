﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellsCreator  {

	private Cell[,] cells_;
	private int numbersOfColumns_;
	private int numbersOfRows_;

	public CellsCreator(int numberOfRows ,int numbersOfColumns){
		numbersOfColumns_ = numbersOfColumns;
		numbersOfRows_ = numberOfRows;
	}

	private void CreateCells_(){
		cells_ = new Cell[numbersOfColumns_,numbersOfRows_];
		for (int row = 0; row < numbersOfRows_; row++) {
			CreateRowOfCells_ (row);
		}
	}

	private void CreateRowOfCells_(int row){
		for (int column = 0; column < numbersOfColumns_; column++) {
			cells_ [column, row] = new Cell ();
		}
	}

	public Cell[,] Create(){
		if(null==cells_){
			CreateCells_ ();
		}
		return cells_;
	}
}
