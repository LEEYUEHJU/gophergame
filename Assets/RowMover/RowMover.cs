﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowMover  {

	private ICellMoverSetter cellMoverSetter_;
	private IGrid grid_;
	private int numberOfColumns_;
	private int numberOfRows_;
	private float speed_;

	public RowMover(ICellMoverSetter cellMoverSetter ,IGrid grid){
		cellMoverSetter_ = cellMoverSetter;
		grid_ = grid;
		numberOfColumns_ = grid.GetNumberOfColumns();
	}

	public void MoveBrickFromRowToRowInColumn(int fromRow,int toRow,int column){
		cellMoverSetter_.MoveBrickFromCellToCell_ (grid_.GetCell (column, fromRow),grid_.GetCell (column,toRow));
		cellMoverSetter_.SetSpeed (speed_);
	}

	public void MoveRowOfBricksFromRowToRow(int fromRow,int toRow){
		for (int column = 0; column < numberOfColumns_; column++) {
			MoveBrickFromRowToRowInColumn (fromRow,toRow,column);
		}
	}

	public void SetSpeed(float speed){
		speed_ = speed;
	}
}

