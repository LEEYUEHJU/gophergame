﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class RowMoverTester {

	private GridMock gridMock_;
	private RowMover rowMover_;
	private CellMoverSetterMock cellMoverSetterMock_;
	private Cell[,] cells_;
	private CellsCreator cellCreator_;
	private int numberOfRows_ = 12;
	private int numberOfColumns_ = 5;
	private Grid grid_;

	[SetUp]
	public void Init(){
		gridMock_ = new GridMock();
		cellCreator_ = new CellsCreator (numberOfRows_, numberOfColumns_);
		cells_ = cellCreator_.Create ();
		grid_ = new Grid (cells_);
		cellMoverSetterMock_ = new CellMoverSetterMock ();
		rowMover_ = new RowMover (cellMoverSetterMock_,grid_);
	}

	[Test]
	public void MoveBrickFromRowToRow_MoveBrickFrom0n11To0n0_ReturnCell0n11nCell0n0(){
		rowMover_.MoveBrickFromRowToRowInColumn(11,0,0);
		Assert.AreEqual (new Cell[]{cells_[0,11],cells_[0,0]},cellMoverSetterMock_.GetFromToCells());
	}

	[Test]
	public void MoveBrickFromRowToRow_MoveOutOfRange_ThrowArgumentException(){
		Assert.Throws<System.ArgumentException> (()=>rowMover_.MoveBrickFromRowToRowInColumn (11,-1,0));
	}
		
}
