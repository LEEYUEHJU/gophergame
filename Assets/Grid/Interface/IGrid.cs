﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGrid  {

	 Cell GetCell(int row,int column);
	 int GetNumberOfColumns();
}
