﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Grid  : IGrid{

	private Cell[,] cells_;
	private int numberOfColumns_;
	private int numberOfRows_;

	public Grid(Cell[,] cells){
		numberOfColumns_ = cells.GetUpperBound (0)+1;
		numberOfRows_ = cells.GetUpperBound (1) + 1;
		cells_ = cells;
		SetCellsPosition_ ();
	}

	public Cell GetCell(int row, int column){
		CheckValidateIndex (row,column);
		return 	cells_ [row, column];
	}

	private void CheckValidateIndex(int row ,int column){
		if(numberOfColumns_<= row || 0>row){
			throw new ArgumentException ();
		}
		if(numberOfRows_ <= column || 0>column){
			throw new ArgumentException ();
		}
	}

	public void Init(){
		for (int i = 0; i < numberOfColumns_; i++) {
			for (int j = 0; j < numberOfRows_; j++) {
				cells_[i,j].RemoveContent();
			}
		}
	}

	public int GetNumberOfColumns(){
		return numberOfColumns_;
	}

	private void SetCellsPosition_(){
		for (int column = 0; column < numberOfColumns_; column++) {
			for (int row = 0; row < numberOfRows_; row++) {
				cells_ [column, row].SetPosition (new Vector3(column,-row,0));
			}
		}
	}

}
