﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class GridTester {

	private Grid grid_;
	private Cell[,] cells_;
	private const int numberOfColumns_ = 5;
	private const int numberOfRows_ = 12;
	private int EmptyCellsCounts_;
	private Brick brick0n0_;
	private Brick brick0n1_;
	private void CreateCells_(int width,int height){
		cells_ = new Cell[width, height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				cells_[i,j] = new Cell();
				cells_ [i, j].SetPosition (new Vector3(i,j,0));
			}
		}
	}

	private void CalculateEmptyCellsCounts_(){
		for (int i = 0; i < numberOfColumns_; i++) {
			for (int j = 0; j < numberOfRows_; j++) {
				if(cells_[i,j].IsEmpty()){
					EmptyCellsCounts_++;
				}
			}
		}
	}

	private void CreateBricks_(){
		GameObject brick0n0Object = new GameObject ();
		brick0n0_ = brick0n0Object.AddComponent<Brick> ();
		GameObject brick0n1Object = new GameObject ();
		brick0n1_ = brick0n1Object.AddComponent<Brick> ();
	}

	[SetUp]
	public void Init(){
		CreateCells_ (numberOfColumns_,numberOfRows_);
		grid_ = new Grid (cells_);
		CreateBricks_ ();
		EmptyCellsCounts_ = 0;
	}

	[Test]
	public void GetCell_GetCell0And0_EqualToCells0n0(){
		Assert.AreEqual (cells_[0,0],grid_.GetCell(0,0));
	}

	[Test]
	public void GetCell_GetCell0And1_EqualToCells0n1(){
		Assert.AreEqual (cells_[0,1],grid_.GetCell(0,1));
	}
		
	[Test]
	public void GetCell_0AndMaxColumnIndex_DoesNotThrowArgumentException(){
		Assert.DoesNotThrow(()=>grid_.GetCell(0,numberOfRows_-1));
	}

	[Test]
	public void GetCell_0AndNumberOfRows_ThrowArgumentException(){
		Assert.Throws<System.ArgumentException>(()=>grid_.GetCell(0,numberOfRows_));
	}

	[Test]
	public void GetCell_MaxRowIndexAnd0_DoesNotThrowArgumentException(){
		Assert.DoesNotThrow(()=>grid_.GetCell(numberOfColumns_-1,0));
	}

	[Test]
	public void GetCell_NumberOfColumnsAnd0_ThrowArgumentException(){
		Assert.Throws<System.ArgumentException>(()=>grid_.GetCell(numberOfColumns_,0));
	}

	[Test]
	public void GetCell_MaxRowIndexAndMaxColumnIndex_DoesNotThrowArgumentException(){
		Assert.DoesNotThrow(()=>grid_.GetCell(numberOfColumns_-1,numberOfRows_-1));
	}

	[Test]
	public void Init_OneCellSetContentOfBrickAndInit_EmptyCellCountIs60(){
		cells_ [0,0].SetContent (brick0n0_);
		grid_.Init ();
		CalculateEmptyCellsCounts_ ();
		Assert.AreEqual (60,EmptyCellsCounts_);
	}

	[Test]
	public void Init_TwoCellsSetContentOfBrickAndInit_EmptyCellCountIs60(){
		cells_ [0,0].SetContent (brick0n0_);
		cells_ [1,0].SetContent (brick0n1_);
		grid_.Init ();
		CalculateEmptyCellsCounts_ ();
		Assert.AreEqual (60,EmptyCellsCounts_);
	}
}