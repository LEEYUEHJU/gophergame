﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class CellMoverTester {

	private Cell sourceCell_;
	private Cell destinationCell_;
	private Brick sourceBrick_;
	private Brick destinationBrick_;
	private CellMover cellMover_;
	private Mover mover_;

	[SetUp]
	public void Init(){
		sourceCell_ = new Cell ();
		destinationCell_ = new Cell ();
		SetCellsPosition_ (new Vector3(0,1,0),new Vector3(0,2,0));
		CreateBricks_ ();
		CreateMover_ ();
		cellMover_ = new CellMover (mover_);
	}

	private void CreateBricks_(){
		GameObject upBrickObject = new GameObject ();
		sourceBrick_ = upBrickObject.AddComponent<Brick> ();
		GameObject downBrickObject = new GameObject ();
		destinationBrick_ = downBrickObject.AddComponent<Brick> ();
	}

	private void SetCellsPosition_(Vector3 upCellPosition,Vector3 downCellPosition){
		sourceCell_.SetPosition (upCellPosition);
		destinationCell_.SetPosition (downCellPosition);
	}

	private void CreateMover_(){
		MoveAlgorithm moveAlgorithmn_ = new MoveAlgorithm ();
		mover_ = new Mover (moveAlgorithmn_);
	}
		
	[Test]
	public void MoveBrickFromCellToCell_Arrived_SourceCellIsEmpty(){
		sourceCell_.SetContent (sourceBrick_);
		cellMover_.MoveBrickFromCellToCell (sourceCell_,destinationCell_);
		mover_.StartToMove ();
		mover_.SetSpeed (1);
		mover_.OnUpdate (1);
		Assert.AreEqual (true,sourceCell_.IsEmpty());
	}

	[Test]
	public void MoveBrickFromCellToCell_Arrived_DestinationCellIsNotEmpty(){
		sourceCell_.SetContent (sourceBrick_);
		cellMover_.MoveBrickFromCellToCell (sourceCell_,destinationCell_);
		mover_.StartToMove ();
		mover_.SetSpeed (1);
		mover_.OnUpdate (1);
		Assert.AreEqual (false,destinationCell_.IsEmpty());
	}

//	[Test]
//	public void MoveBrickFromCellToCell_DestinationCellsNotEmptyBeforeMove_ThrowArgumentException(){
//		destinationCell_.SetContent (destinationBrick_);
//		Assert.Throws<System.ArgumentException> (()=>cellMover_.MoveBrickFromCellToCell (sourceCell_,destinationCell_));
//	}

	[Test]
	public void MoveBrickFromCellToCell_SourceCellIsEmptyBeforeMove_ThrowArgumentException(){
		Assert.Throws<System.ArgumentException> (()=>cellMover_.MoveBrickFromCellToCell (sourceCell_,destinationCell_));
	}
}
