﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class CellMover : ICellMover{

	private Cell fromCell_;
	private Cell toCell_;
	private Mover mover_;

	private Action<CellMover> recycleEvent_;

	public CellMover(Mover mover){
		mover_ = mover;
		RegisterMoverArriveEvents_ ();
	}

	public void SetSpeed(float speed){
		mover_.SetSpeed (speed);
	}

	private void SetTransform_(Cell cell){
		Brick brick = cell.GetContent ();
		mover_.SetTransform (brick.transform);
	}

	private void SetFromPositionToPosition_(Cell fromCell, Cell toCell){
		mover_.SetFromPosition (fromCell.GetPosition());
		mover_.SetDestination (toCell.GetPosition());
	}

	private void RegisterMoverArriveEvents_(){
		mover_.RegisterArrivedEvent (UpdateCellsData_);
		mover_.RegisterArrivedEvent (PrintState_);
		mover_.RegisterArrivedEvent (OnRecycle_);
	}

	public void RegisterArriveEvent(Action<CellMover> arrivedEvent){
		recycleEvent_ = arrivedEvent;
	}

	private void OnRecycle_(){
		if(null!=recycleEvent_){
			recycleEvent_.Invoke (this);
		}
	}

	private  void UpdateCellsData_(){
		if(null!=fromCell_&&null!=toCell_){
			Brick brickFromSource = fromCell_.GetContent ();
			fromCell_.RemoveContent ();
			toCell_.SetContent (brickFromSource);	
		}
	}

	private void ValidationCheck_(Cell fromCell, Cell toCell){
		if(fromCell.IsEmpty()){
			throw new ArgumentException ();
		}
	}

	public void MoveBrickFromCellToCell(Cell fromCell, Cell toCell){
		ValidationCheck_ (fromCell,toCell);
		fromCell_ = fromCell;
		toCell_ = toCell;
		SetTransform_ (fromCell);
		SetFromPositionToPosition_ (fromCell,toCell);
	}
		
	private void PrintState_(){
		if(null!=fromCell_&&null!=toCell_){
			Debug.Log ("sourceCell ID: "+fromCell_.GetID()+" is Empty ?"+fromCell_.IsEmpty());
			Debug.Log ("destinationCell ID: "+toCell_.GetID()+" is Empty ?"+toCell_.IsEmpty());	
		}
	}

	public Mover GetMover(){
		return mover_;
	}
}
