﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICellMover  {

	void SetSpeed (float speed);
	void MoveBrickFromCellToCell(Cell fromCell,Cell toCell);
}
