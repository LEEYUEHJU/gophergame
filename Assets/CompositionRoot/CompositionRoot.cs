﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompositionRoot : MonoBehaviour {
	private Cell[,]cells_;
	private const int numberOfColumns_ = 5;
	private const int numbersOfRows_ = 12;
	private Brick[,] bricks_;
	private Brick[]bricksInSecondRow_;
	private Brick[] bricksInThirdRow_;
	private Brick[] addedBricksOfRow_;
	private Brick[]bricksInFirstColumn_;
	private Grid grid_;
	private Mover[] movers_;
	private const int numbersOfMovers_ = 10;
	private float moverSpeed_ = 5;
	[SerializeField]
	private GameObject brickPrefab_;
	private MoveAlgorithm moveAlgorithm_;
	private CellMover [] cellMovers_;
	private CellsCreator cellsCreator_;
	private BricksCreator bricksCreator_;
	private RowMover rowMover_;
	private CellMoversQueue cellMoverQueue_;
	private CellMoversDistributor cellMoverDistributor_;
	private ICellMoverSetter cellMoverSetter_;
	public void Awake(){
		BuildScene_ ();
	}

	private void BuildScene_(){
		moveAlgorithm_ = new MoveAlgorithm ();
		cellsCreator_ = new CellsCreator (numbersOfRows_,numberOfColumns_);
		bricksCreator_ = new BricksCreator (brickPrefab_,numbersOfRows_,numberOfColumns_);
		cells_ = cellsCreator_.Create ();
		grid_ = new Grid (cells_);
		SetUpCells_ ();
		CreateBricks_ ();
		SetBricksIntoCells_ ();
		CreateMovers_ ();
		CreateCellMovers_ ();
		cellMoverQueue_ = new CellMoversQueue (cellMovers_);
		cellMoverDistributor_ = new CellMoversDistributor (cellMoverQueue_,cellMovers_);
		cellMoverSetter_ = new CellMoverSetter (cellMoverDistributor_);
		rowMover_ = new RowMover (cellMoverSetter_, grid_);
		rowMover_.SetSpeed (moverSpeed_);
		MoveRows_ ();
	}

	private void SetUpCells_(){
		SetCellsID_ ();
	}

	private void MoveRows_(){
		rowMover_.MoveRowOfBricksFromRowToRow (1, 0);
		rowMover_.MoveRowOfBricksFromRowToRow (2, 1);
	}
		
	private void SetCellsID_(){
		for (int column = 0; column < numberOfColumns_; column++) {
			for (int row = 0; row < numbersOfRows_; row++) {
				cells_ [column, row].SetID (column.ToString()+","+row.ToString());
			}
		}
	}

	private void CreateBricks_(){
		bricksInSecondRow_ = bricksCreator_.CreateRow ();
		bricksInThirdRow_ = bricksCreator_.CreateRow ();
	}
		
	private void SetBricksIntoCells_(){
		SetBricksIntoRowOfCell_(bricksInSecondRow_,1);
		SetBricksIntoRowOfCell_(bricksInThirdRow_,2);
	}
		
	public void SetBricksIntoRowOfCell_(Brick[]bricks,int row){
		for (int column = 0; column < bricks.Length; column++) {
			cells_ [column, row].SetContent (bricks[column]);
			bricks [column].SetPosition (new Vector3(column,-row,0));
		}
	}

	private void CreateMovers_(){
		movers_ = new Mover[numbersOfMovers_];
		for (int i = 0; i < movers_.Length; i++) {
			movers_ [i] = new Mover (moveAlgorithm_);
		}
	}

	private void CreateCellMovers_(){
		cellMovers_ = new CellMover[numbersOfMovers_];
		for (int i = 0; i < cellMovers_.Length; i++) {
			cellMovers_ [i] = new CellMover (movers_[i]);
		}
	}

	private void StartMovers_(){
		for (int i = 0; i < movers_.Length; i++) {
			movers_ [i].StartToMove ();		
		}
	}

	void Update () {
		for (int i = 0; i < movers_.Length; i++) {
			movers_ [i].OnUpdate (Time.deltaTime);
		}
		if(Input.GetKeyDown(KeyCode.UpArrow)){
			StartMovers_ ();
		}
	}
}
